package com.aliens.invension

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.compose.runtime.livedata.observeAsState
import androidx.compose.ui.platform.setContent
import com.aliens.invension.ui.EnemyUi
import com.aliens.invension.ui.ResultUi
import com.aliens.invension.ui.VolcanoUi
import com.aliens.invension.ui.MenuUi
import com.aliens.invension.ui.utils.AliensInvensionTheme

class GameActivity : AppCompatActivity() {

    private val viewModel: GameViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.finish = this::finish
        setContent {
            AliensInvensionTheme {
                viewModel.gameScoreLiveData.value?.let { score -> ResultUi(score = score) }
                viewModel.entityLiveData.value?.let { game -> EnemyUi(item = game) }
                viewModel.volcanoLiveData.value?.let { volcano -> VolcanoUi(item = volcano) }
                viewModel.closableLiveData.observeAsState().value?.let { menu -> MenuUi(list = menu) }
            }
        }
    }

    override fun onBackPressed() {
        viewModel.onBackPressed()
    }
}
