package com.aliens.invension.ui

import android.content.Context
import android.webkit.JavascriptInterface

private const val ALIENS_TABLE = "com.aliens.table.123"
private const val ALIENS_ARGS = "com.aliens.value.456"

class JavaScript(
    private val callback: Callback
) {

    interface Callback {
        fun needAuth()
        fun authorized()
    }

    @JavascriptInterface
    fun onAuthorized() {
        callback.authorized()
    }

    @JavascriptInterface
    fun onNeedAuth() {
        callback.needAuth()
    }
}

fun Context.saveLink(deepArgs: String) {
    val sharedPreferences = getSharedPreferences(ALIENS_TABLE, Context.MODE_PRIVATE)
    sharedPreferences.edit().putString(ALIENS_ARGS, deepArgs).apply()
}

fun Context.getArgs(): String? {
    val sharedPreferences = getSharedPreferences(ALIENS_TABLE, Context.MODE_PRIVATE)
    return sharedPreferences.getString(ALIENS_ARGS, null)
}
