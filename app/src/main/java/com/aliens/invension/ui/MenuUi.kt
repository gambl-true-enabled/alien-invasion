package com.aliens.invension.ui

import androidx.compose.animation.transition
import androidx.compose.foundation.Image
import androidx.compose.foundation.Text
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.*
import androidx.compose.foundation.layout.ColumnScope.gravity
import androidx.compose.foundation.layout.RowScope.gravity
import androidx.compose.material.EmphasisAmbient
import androidx.compose.material.ProvideEmphasis
import androidx.compose.material.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.graphics.Color
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.res.vectorResource
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import com.aliens.invension.R
import com.aliens.invension.ui.utils.fontFamily

@Composable
fun MenuUi(
    list: List<ClosableItem>
) {
    Surface(
        modifier = Modifier.fillMaxHeight()
            .fillMaxWidth(),
        color = Color.Transparent
    ) {
        Column(
            modifier = Modifier.wrapContentHeight()
                .fillMaxWidth()
                .gravity(Alignment.CenterVertically)
        ) {
            list.forEach { ClosableItemUi(item = it) }
        }
    }
}

@Composable
private fun ClosableItemUi(
    item: ClosableItem
) {
    val transition = transition(
        definition = getButtonTransitionDefinition,
        toState = item.state.value,
        onStateChangeFinished = {
            if (it != MenuItemState.OPEN)
                item.animationFinished.invoke()
        }
    )
    Surface(
        modifier = Modifier.fillMaxWidth()
            .offset(x = transition[menuItemHorizontalOffset]),
        color = Color.Transparent
    ) {
        when(item) {
            is ScoreTextModel -> ScoreUi(scoreTextModel = item)
            is ButtonModel -> ButtonUi(buttonModel = item)
        }
    }
}

@Composable
private fun ScoreUi(
    scoreTextModel: ScoreTextModel
) {
    Text(
        text = "your score: ${scoreTextModel.score.value}",
        letterSpacing = 2.sp,
        fontFamily = fontFamily,
        fontSize = 36.sp,
        modifier = Modifier.wrapContentWidth()
            .padding(vertical = 28.dp)
            .gravity(Alignment.CenterHorizontally)
    )
}

@Composable
private fun ButtonUi(
    buttonModel: ButtonModel
) {
    Stack(
        modifier = Modifier.padding(26.dp)
            .clickable(onClick = buttonModel.clickCallback)
    ) {
        Image(
            asset = vectorResource(id = R.drawable.ic_button_back),
            modifier = Modifier.gravity(Alignment.Center)
        )
        ProvideEmphasis(emphasis = EmphasisAmbient.current.high) {
            Text(
                text = stringResource(id = buttonModel.text.value),
                letterSpacing = 2.sp,
                fontFamily = fontFamily,
                fontSize = 32.sp,
                modifier = Modifier.gravity(Alignment.Center)
            )
        }
    }
}