package com.aliens.invension.ui

import androidx.compose.animation.animate
import androidx.compose.animation.core.FastOutSlowInEasing
import androidx.compose.animation.core.LinearEasing
import androidx.compose.animation.transition
import androidx.compose.foundation.Image
import androidx.compose.ui.gesture.pressIndicatorGestureFilter
import androidx.compose.foundation.Text
import androidx.compose.foundation.layout.*
import androidx.compose.ui.Alignment
import androidx.compose.runtime.*
import androidx.compose.ui.Modifier
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.IntSize
import androidx.compose.ui.unit.dp
import androidx.compose.ui.unit.sp
import androidx.compose.ui.onPositioned
import androidx.compose.animation.core.transitionDefinition
import androidx.compose.animation.core.tween
import androidx.compose.ui.platform.DensityAmbient
import androidx.compose.ui.res.imageResource
import com.aliens.invension.ui.utils.fontFamily
import kotlin.math.min
import kotlin.random.Random

@Composable
fun ResultUi(
    score: Int
) {
    Column(
        modifier = Modifier
            .fillMaxWidth()
            .wrapContentHeight()
    ) {
        Text(
            text = "score: $score",
            fontFamily = fontFamily,
            fontSize = 24.sp,
            modifier = Modifier
                .padding(25.dp)
                .gravity(Alignment.End)
        )
    }
}

@Composable
fun EnemyUi(
    item: EnemyEntity
) {
    var screenSize by remember { mutableStateOf(IntSize.Zero) }
    Stack(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
            .onPositioned { screenSize = it.size }
    ) {
        if (screenSize.height > 0 && screenSize.width > 0) {
            val minDimension = min(screenSize.width, screenSize.height)
            val enemySize = ((minDimension*0.2)/DensityAmbient.current.density).dp
            val volcanoSize = ((minDimension*0.8*0.5)/ DensityAmbient.current.density).dp
            val enemyY = item.getYStartInDp(
                DensityAmbient.current.density,
                enemySize,
                screenSize.height,
                volcanoSize
            )
            val enemyX = item.getXStartInDp(
                DensityAmbient.current.density,
                enemySize,
                screenSize.width,
                enemyY
            )
            val destY = (screenSize.height/ DensityAmbient.current.density).dp - volcanoSize - enemySize
            val destX = (screenSize.width/2/ DensityAmbient.current.density).dp - enemySize/2
            val enemyTransitionDef = transitionDefinition<EnemyItemState> {
                state(EnemyItemState.TRIPPING) {
                    this[enemyItemXOffset] = destX
                    this[enemyItemYOffset] = destY
                    this[alphaItemOffset] = 1.0f
                }
                state(EnemyItemState.DEAD) {
                    this[enemyItemXOffset] = enemyX
                    this[enemyItemYOffset] = enemyY
                    this[alphaItemOffset] = 0.0f
                }
                state(EnemyItemState.CREATED) {
                    this[enemyItemXOffset] = enemyX
                    this[enemyItemYOffset] = enemyY
                    this[alphaItemOffset] = 1.0f
                }
            }
            val transition = transition(
                definition = enemyTransitionDef,
                toState = item.state.value
            )
            val animSpec = tween<Dp>(
                durationMillis = Random.nextInt(600, 1500),
                easing = if (Random.nextBoolean()) FastOutSlowInEasing else LinearEasing
            )
            Image(
                asset = imageResource(id = item.icon),
                modifier = Modifier
                    .offset(
                        y = animate(target = transition[enemyItemYOffset], animSpec) {
                            if (item.state.value != EnemyItemState.DEAD)
                                item.loseCallback.invoke(item)
                        },
                        x = animate(target = transition[enemyItemXOffset], animSpec) {
                            if (item.state.value != EnemyItemState.DEAD)
                                item.loseCallback.invoke(item)
                        }
                    )
                    .size(enemySize)
                    .pressIndicatorGestureFilter(
                        onStart = {
                            if (item.state.value != EnemyItemState.DEAD)
                                item.state.value = EnemyItemState.DEAD
                        }
                    ),
                alpha = animate(
                    target = transition[alphaItemOffset],
                    animSpec = tween(
                        durationMillis = 100,
                        easing = if (Random.nextBoolean()) FastOutSlowInEasing else LinearEasing
                    ),
                    endListener = {
                        if (item.state.value == EnemyItemState.DEAD)
                            item.killEnemyCallback.invoke(item)
                    }
                )
            )
        }
    }
}

@Composable
fun VolcanoUi(
    item: VolcanoEntity
) {
    var screenSizeVolcano by remember { mutableStateOf(IntSize.Zero) }
    Stack(
        modifier = Modifier
            .fillMaxHeight()
            .fillMaxWidth()
            .onPositioned { screenSizeVolcano = it.size }
    ) {
        if (screenSizeVolcano.height > 0 && screenSizeVolcano.width > 0) {
            val minDimension = min(screenSizeVolcano.width, screenSizeVolcano.height)
            val volcanoSize = ((minDimension*0.8)/ DensityAmbient.current.density).dp
            if (volcanoSize > 0.dp) {
                Image(
                    asset = imageResource(id = item.icon),
                    modifier = Modifier
                        .size(volcanoSize)
                        .offset(
                            y = (screenSizeVolcano.height/ DensityAmbient.current.density).dp - volcanoSize,
                            x = (screenSizeVolcano.width/2/ DensityAmbient.current.density).dp - volcanoSize/2
                        )
                )
            }
        }
    }
}
