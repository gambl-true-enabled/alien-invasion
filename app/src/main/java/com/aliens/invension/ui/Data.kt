package com.aliens.invension.ui

import androidx.compose.animation.DpPropKey
import androidx.compose.animation.core.FloatPropKey
import androidx.compose.animation.core.transitionDefinition
import androidx.compose.runtime.MutableState
import androidx.compose.runtime.Stable
import androidx.compose.runtime.mutableStateOf
import androidx.compose.ui.unit.Dp
import androidx.compose.ui.unit.dp
import com.aliens.invension.R
import kotlin.random.Random

interface ClosableItem {
    val state: MutableState<MenuItemState>
    var animationFinished: () -> Unit
}

data class ScoreTextModel(
    override val state: MutableState<MenuItemState> = mutableStateOf(MenuItemState.CLOSE_LEFT),
    override var animationFinished: () -> Unit = {},
    val score: MutableState<Int>
): ClosableItem

data class ButtonModel(
    override val state: MutableState<MenuItemState> = mutableStateOf(MenuItemState.CLOSE_LEFT),
    override var animationFinished: () -> Unit = {},
    var text: MutableState<Int> = mutableStateOf(R.string.app_name),
    var clickCallback: () -> Unit = {}
): ClosableItem

interface GameEntity

data class VolcanoEntity(val icon: Int): GameEntity

data class EnemyEntity(val icon: Int,
                       val killEnemyCallback: (item: EnemyEntity) -> Unit,
                       val loseCallback: (item: EnemyEntity) -> Unit,
                       var state: MutableState<EnemyItemState> = mutableStateOf(EnemyItemState.CREATED),
                       private val xRand: Boolean = Random.nextBoolean(),
                       private val yStart: Int = coordinateRange.random(),
                       private val xStart: Int = coordinateRange.random()): GameEntity {

    @Stable
    fun getXStartInDp(dens: Float, enemySize: Dp,
                      screenWidthPx: Int, yInDp: Dp): Dp {
        val screenWidthDp = (screenWidthPx/dens).dp
        if (yInDp > 0.dp) {
            return if (xRand)
                -enemySize
            else
                screenWidthDp
        }
        return ((xStart*screenWidthDp.value)/100).dp
    }

    @Stable
    fun getYStartInDp(dens: Float, enemySize: Dp,
                      screenHeightPx: Int, volcanoHeightDp: Dp): Dp {
        return ((yStart*(((screenHeightPx/dens).dp) - volcanoHeightDp).value)/100).dp - enemySize
    }
}

val coordinateRange = IntRange(0, 100)

enum class EnemyItemState {
    CREATED,
    DEAD,
    TRIPPING
}

enum class MenuItemState {
    OPEN,
    CLOSE_LEFT,
    CLOSE_RIGHT
}

val menuItemHorizontalOffset = DpPropKey()
val enemyItemXOffset = DpPropKey()
val enemyItemYOffset = DpPropKey()
val alphaItemOffset = FloatPropKey()

val getButtonTransitionDefinition = transitionDefinition<MenuItemState> {
    state(MenuItemState.OPEN) {
        this[menuItemHorizontalOffset] = 0.dp
    }
    state(MenuItemState.CLOSE_LEFT) {
        this[menuItemHorizontalOffset] = (-1 * 800).dp
    }
    state(MenuItemState.CLOSE_RIGHT) {
        this[menuItemHorizontalOffset] = 800.dp
    }
}
