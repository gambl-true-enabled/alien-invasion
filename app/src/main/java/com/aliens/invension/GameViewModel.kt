package com.aliens.invension

import android.os.Handler
import androidx.compose.runtime.State
import androidx.compose.runtime.mutableStateOf
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.aliens.invension.ui.EnemyEntity
import com.aliens.invension.ui.EnemyItemState
import com.aliens.invension.ui.*
import com.aliens.invension.ui.VolcanoEntity
import kotlin.random.Random

class GameViewModel: ViewModel() {

    private val _menuLiveData = MutableLiveData<List<ClosableItem>>()
    private val _entityLiveData = mutableStateOf<EnemyEntity?>(null)
    private val _volcanoLiveData = mutableStateOf<VolcanoEntity?>(null)
    private val _gameScoreLiveData = mutableStateOf<Int?>(null)

    val closableLiveData: LiveData<List<ClosableItem>> = _menuLiveData
    val entityLiveData: State<EnemyEntity?> = _entityLiveData
    val volcanoLiveData: State<VolcanoEntity?> = _volcanoLiveData
    val gameScoreLiveData: State<Int?> = _gameScoreLiveData

    private lateinit var backPress: () -> Unit
    private lateinit var screen: Screen

    private val mainClosables: List<ClosableItem>
    private val resultClosables: List<ClosableItem>

    private val scoreState = mutableStateOf(0)

    private var scoreRaw = 0

    lateinit var finish: () -> Unit

    private enum class Screen {
        MAIN,
        GAME,
        RESULT
    }

    init {
        resultClosables = listOf(
            ScoreTextModel(
                score = scoreState
            ),
            ButtonModel(
                text = mutableStateOf(R.string.retry),
                clickCallback = this@GameViewModel::hideResultMenu,
                animationFinished =  this@GameViewModel::startGame
            )
        )

        mainClosables = listOf(
            ButtonModel(
                text = mutableStateOf(R.string.play_game),
                clickCallback = this@GameViewModel::hideMainMenu,
                animationFinished =  this@GameViewModel::startGame
            ),
            ButtonModel(
                text = mutableStateOf(R.string.exit),
                clickCallback = {
                    finish()
                }
            )
        )

        flushMainMenu()
    }

    private fun generateEnemy() {
        if (screen != Screen.GAME)
            return
        val enemyEntity = EnemyEntity(R.drawable.ic_alien_enemy,
            {
                scoreRaw++
                _gameScoreLiveData.value = scoreRaw
                removeFromGame()
                generateEnemy()
            }
            ,
            {
                if (_entityLiveData.value == null)
                    return@EnemyEntity
                _entityLiveData.value = null
                _volcanoLiveData.value = null
                scoreState.value = scoreRaw
                flushResultMenu()
            }
        )
        enemyEntity.addToGame()
        Handler().postDelayed({
            if (screen != Screen.GAME) return@postDelayed
            enemyEntity.state.value = EnemyItemState.TRIPPING
        }, 300)
    }

    private fun flushMainMenu() {
        _gameScoreLiveData.value = null
        screen = Screen.MAIN
        _menuLiveData.value = mainClosables
        backPress = (mainClosables[1] as ButtonModel).clickCallback
        mainClosables.forEach { it.show() }
    }

    private fun hideMainMenu() {
        mainClosables.forEach { it.hide() }
    }

    private fun startGame() {
        screen = Screen.GAME
        scoreRaw = 0
        _gameScoreLiveData.value = scoreRaw
        backPress = {
            _entityLiveData.value = null
            _volcanoLiveData.value = null
            flushMainMenu()
        }
        if (_volcanoLiveData.value == null)
            _volcanoLiveData.value = VolcanoEntity(R.drawable.ic_volcano)
        startEnemyGenerator()
    }

    private fun flushResultMenu() {
        _gameScoreLiveData.value = null
        screen = Screen.RESULT
        _menuLiveData.value = resultClosables
        val button = (resultClosables[1] as ButtonModel)
        backPress = {
            button.animationFinished = {
                flushMainMenu()
                button.animationFinished = this::startGame
            }
            button.clickCallback.invoke()
        }
        resultClosables.forEach { it.show() }
    }

    private fun hideResultMenu() {
        _volcanoLiveData.value = null
        resultClosables.forEach { it.hide() }
    }

    private fun ClosableItem.hide() {
        if (Random.nextBoolean())
            this.state.value = MenuItemState.CLOSE_LEFT
        else
            this.state.value = MenuItemState.CLOSE_RIGHT
    }

    private fun ClosableItem.show() {
        this.state.value = MenuItemState.OPEN
    }

    private fun startEnemyGenerator() {
        generateEnemy()
    }

    private fun EnemyEntity.addToGame() {
        _entityLiveData.value = this
    }

    private fun removeFromGame() {
        _entityLiveData.value = null
    }

    fun onBackPressed() {
        backPress.invoke()
    }
}